package ru.ifmo.ctddev.katunina.MLogic_one_Verify_proof;

/**
 * Created by Евгения on 06.09.14.
 */
public class Conjunction extends BinaryOperation {
    public Conjunction(Expression parsedExpression, Expression expression) {
        super(parsedExpression, expression);
        sign=getSign();
    }

    @Override
    public String getSign() {
        return "&";
    }
}
