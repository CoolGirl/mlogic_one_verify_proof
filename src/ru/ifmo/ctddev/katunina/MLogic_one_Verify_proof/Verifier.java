package ru.ifmo.ctddev.katunina.MLogic_one_Verify_proof;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * Created by Евгения on 06.09.14.
 */
public class Verifier {
    static BufferedReader br;
    static HashMap<Integer, Expression> axiomSchemes;
    private HashMap<Expression, List<Expression>> implicationListByRightOperand;
    private HashMap<Expression, Integer> expressionHashMap;
    private Set<Expression> assumptions = new HashSet<Expression>();

    static {
        try {
            br = new BufferedReader(new FileReader("axiom schemes.txt"));
            axiomSchemes = new HashMap<Integer, Expression>();
            String currentExpression = br.readLine();
            int currentAxiomScheme = 1;
            do {
                axiomSchemes.put(currentAxiomScheme, ExpressionParser.tryParseExpression(currentExpression, currentAxiomScheme));
                currentExpression = br.readLine();
                currentAxiomScheme++;
            } while (currentExpression != null);
        } catch (IOException e) {
        } catch (ExpressionParser.WrongExpressionException w) {
        }
    }

    public Verifier(Set<Expression> assumptions){
          implicationListByRightOperand = new HashMap<Expression, List<Expression>>();
          expressionHashMap = new HashMap<Expression, Integer>();
          this.assumptions = assumptions;
    }

    VerifyingResult tryModusPonens(Expression parsed) {
        if (implicationListByRightOperand.containsKey(parsed))
            for (Expression expression : implicationListByRightOperand.get(parsed)) {
                if (expressionHashMap.containsKey(((Implication) expression).left))
                    return new VerifyingResult(expressionHashMap.get(((Implication) expression).left),
                            expressionHashMap.get(expression));
            }
        return new VerifyingResult();
    }

    VerifyingResult verifyExpression(Expression parsed, int expressionNumber) {
        boolean result;
        if (parsed instanceof Implication)
            if (implicationListByRightOperand.containsKey(((Implication) parsed).right))
                implicationListByRightOperand.get(((Implication) parsed).right).add(parsed);
            else {
                List<Expression> auxList = new ArrayList<Expression>();
                auxList.add(parsed);
                implicationListByRightOperand.put(((Implication) parsed).right, auxList);
            }
        if (!expressionHashMap.containsKey(parsed))
            expressionHashMap.put(parsed, expressionNumber);
        for (int key : axiomSchemes.keySet()) {
            Expression value = axiomSchemes.get(key);
            HashMap<String, Expression> variable = new HashMap<String, Expression>();
            result = value.isTemplateFor(parsed, variable);
            if (result) return new VerifyingResult(key);
        }
        if (assumptions.contains(parsed))
            return new VerifyingResult(parsed);
        return tryModusPonens(parsed);
    }
}
