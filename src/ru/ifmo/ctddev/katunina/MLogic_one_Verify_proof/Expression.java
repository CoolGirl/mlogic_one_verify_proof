package ru.ifmo.ctddev.katunina.MLogic_one_Verify_proof;

import java.util.HashMap;

/**
 * Created by Евгения on 04.09.14.
 */
public abstract class Expression {
    String stringExpression =null;

     abstract boolean isTemplateFor(Expression formToCompare, HashMap<String, Expression> variable);
    protected abstract String getStringRepresentation();

    public String toString() {
        if (stringExpression == null)
            stringExpression = getStringRepresentation();
        return stringExpression;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Expression && this.toString().equals(obj.toString());
    }

    @Override
    public int hashCode() {
        return toString().hashCode();
    }
}
