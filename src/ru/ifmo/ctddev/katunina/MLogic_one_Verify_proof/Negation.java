package ru.ifmo.ctddev.katunina.MLogic_one_Verify_proof;

import java.util.HashMap;

/**
 * Created by Евгения on 07.09.14.
 */
public class Negation extends Expression {
    private Expression operand;
   private  String sign;

    public Negation (Expression operand){
        this.operand = operand;
        sign = getSign();
    }

    @Override
    boolean isTemplateFor(Expression formToCompare, HashMap<String, Expression> variable) {
        return formToCompare instanceof Negation && this.operand.isTemplateFor(((Negation) formToCompare).operand, variable);
    }

   public String getSign(){
       return      "!";
   }

    @Override
    protected String getStringRepresentation() {
        return sign + (!(operand instanceof PropositionalVariable) && !(operand instanceof Negation) ? "(" : "")
                + operand.getStringRepresentation() +
                (!(operand instanceof PropositionalVariable) && !(operand instanceof Negation) ? ")" : "");
    }
}
