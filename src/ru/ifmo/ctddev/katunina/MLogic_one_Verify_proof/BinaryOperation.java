package ru.ifmo.ctddev.katunina.MLogic_one_Verify_proof;

import java.util.HashMap;

/**
 * Created by Евгения on 06.09.14.
 */
public abstract class BinaryOperation extends Expression {
    protected String sign;
    protected Expression left, right;

    static HashMap<String, Integer> priorityOperation;

    static {
        priorityOperation = new HashMap<String, Integer>();
        priorityOperation.put("&", 1);
        priorityOperation.put("|", 2);
        priorityOperation.put("->", 3);
    }

    protected boolean isThisPriorityHigher(String signOne, String signOther) {
        return priorityOperation.get(signOne) < priorityOperation.get(signOther);
    }

    public BinaryOperation(Expression left, Expression right) {
        this.left = left;
        this.right = right;
    }

    public abstract String getSign();

    @Override
    boolean isTemplateFor(Expression formToCompare, HashMap<String, Expression> variable) {
        return ((formToCompare.getClass().equals(this.getClass())
                && this.left.isTemplateFor(((BinaryOperation) formToCompare).left, variable)
                && this.right.isTemplateFor((((BinaryOperation) formToCompare).right), variable)));
    }

    @Override
    protected String getStringRepresentation() {
        boolean leftBrackets = false, rightBrackets = false;
        if (left instanceof BinaryOperation && isThisPriorityHigher(sign, ((BinaryOperation) left).getSign()))
            leftBrackets = true;
        if (right instanceof BinaryOperation
                && (isThisPriorityHigher(sign, ((BinaryOperation) right).getSign())) || right.getClass().equals(getClass()))
            rightBrackets = true;
        return ((leftBrackets ? "(" : "")
                + left.toString()
                + (leftBrackets ? ")" : "")
                + sign
                + (rightBrackets ? "(" : "")
                + right.toString()
                + (rightBrackets ? ")" : ""));
    }
}
