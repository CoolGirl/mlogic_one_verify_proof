package ru.ifmo.ctddev.katunina.MLogic_one_Verify_proof;

/**
 * Created by Евгения on 06.09.14.
 */
public class Disjunction extends BinaryOperation {
    public Disjunction(Expression parsedExpression, Expression expression) {
        super(parsedExpression, expression);
        sign=getSign();
    }

    @Override
    public String getSign() {
        return "|";
    }
}
