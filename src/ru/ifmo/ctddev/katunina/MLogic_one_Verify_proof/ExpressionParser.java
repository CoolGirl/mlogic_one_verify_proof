package ru.ifmo.ctddev.katunina.MLogic_one_Verify_proof;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Евгения on 04.09.14.
 */
public class ExpressionParser {

    int currentIndex;
    String expression;
    int stringNumber;
    static List<String> alphabet;
    static {
        alphabet = new ArrayList<String>();
        char currentLetter='A';
        while (currentLetter<='Z'){
            alphabet.add(Character.toString(currentLetter++)) ;
        }
    }



    static class WrongExpressionException extends Exception {
        WrongExpressionException(ExpressionParser parser) {
            super("Invalid expression "
                    + parser.expression
                    + "at number"
                    + parser.stringNumber
                    + " at position "
                    + parser.currentIndex);
        }
    }

    private ExpressionParser(String userExpression, int stringNumber) {
        currentIndex = 0;
        expression = userExpression;
        this.stringNumber = stringNumber;
    }

    static Expression tryParseExpression(String userExpression, int stringNumber) throws WrongExpressionException {
        ExpressionParser parser = new ExpressionParser(userExpression, stringNumber);
        Expression result = parser.parseExpression();
        if (parser.currentIndex != parser.expression.length())
            throw new WrongExpressionException(parser);
        return result;
    }

    Expression parseExpression() throws WrongExpressionException {
        Expression parsedExpression = parseDisjunction();
        ignoreWhitespaces();
        if (currentIndex < expression.length() && expression.charAt(currentIndex) == '-')   {
            currentIndex++;
            if (currentIndex < expression.length() && expression.charAt(currentIndex) == '>') {
                currentIndex++;
                parsedExpression = new Implication(parsedExpression, parseExpression());
            }
            else
                throw new WrongExpressionException(this);
        }
        return parsedExpression;
    }

    Expression parseDisjunction() throws WrongExpressionException {
        Expression parsedExpression = parseConjunction();
        ignoreWhitespaces();
        while (currentIndex < expression.length() && expression.charAt(currentIndex) == '|') {
            currentIndex++;
            parsedExpression = new Disjunction(parsedExpression, parseConjunction());
            ignoreWhitespaces();
        }
        return parsedExpression;
    }

    Expression parseConjunction() throws WrongExpressionException {
        ignoreWhitespaces();
        Expression parsedExpression = parseNegation();
        ignoreWhitespaces();
        while (currentIndex < expression.length() && expression.charAt(currentIndex) == '&'){
            currentIndex++;
            parsedExpression = new Conjunction(parsedExpression, parseNegation());
            ignoreWhitespaces();
        }
        return parsedExpression;
    }

    Expression parseNegation() throws WrongExpressionException {
        ignoreWhitespaces();
        Expression parsedExpression = null;
        if (currentIndex < expression.length()) {
            if (expression.charAt(currentIndex) == '!'){
                currentIndex++;
                parsedExpression = new Negation(parseNegation());
            }
            else if (expression.charAt(currentIndex) == '(') {
                currentIndex++;
                parsedExpression = parseExpression();
                ignoreWhitespaces();
                if (!(currentIndex < expression.length() && expression.charAt(currentIndex) == ')'))
                    throw new WrongExpressionException(this);
                else
                    currentIndex++;
            } else {
                String variable = tryConsumePropositionalVariable();
                if (variable != null)
                    parsedExpression = new PropositionalVariable(variable);
            }
        }
        if (parsedExpression == null)
            throw new WrongExpressionException(this);
        return parsedExpression;
    }

    String tryConsumePropositionalVariable() {
        int begin = currentIndex;
        String propositionalVariableName;
        if (alphabet.contains(Character.toString(expression.charAt(currentIndex)))){
            currentIndex++;
            while (currentIndex<expression.length()&&Character.isDigit(expression.charAt(currentIndex)))
                currentIndex++;
            return (expression.substring(begin,currentIndex));
        }
        return null;
    }

    void ignoreWhitespaces() {
        while (currentIndex < expression.length() && Character.isWhitespace(expression.charAt(currentIndex))) {
            currentIndex++;
        }
    }
}
