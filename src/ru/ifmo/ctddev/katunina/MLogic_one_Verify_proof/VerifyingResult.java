package ru.ifmo.ctddev.katunina.MLogic_one_Verify_proof;

/**
 * Created by Евгения on 07.09.14.
 */
public class VerifyingResult {
    private String result;
    private boolean correctVerifying = true;

    public VerifyingResult() {
        result = "Не доказано";
        correctVerifying = false;
    }

    public VerifyingResult(int axiomNumber) {
        result = "Сх. акс. " + axiomNumber;
    }

    public VerifyingResult(int expressionNumber, int otherExpressionNumber) {
        result = "M.P. " + expressionNumber + ", " + otherExpressionNumber;
    }

    public VerifyingResult(Expression assumption){
        result = "Предположение " + assumption;
    }

    public boolean isVerifyingCorrect(){
         return correctVerifying;
    }
    @Override
    public String toString() {
        return result;
    }
}
