package ru.ifmo.ctddev.katunina.MLogic_one_Verify_proof;

import java.io.*;
import java.util.HashSet;
import java.util.Set;

public class Main {

    public static void main(String[] args) throws IOException, ExpressionParser.WrongExpressionException {
        BufferedReader br = new BufferedReader(new FileReader("input.txt"));
        PrintWriter pw = new PrintWriter("output.txt","UTF-8");
        String currentExpression = br.readLine();
        currentExpression = currentExpression.replaceAll("\\s+", "");
        int stringNumber = 1;
        Set<Expression> assumptions = new HashSet<Expression>();
        assumptions.add(new Negation(new PropositionalVariable("A")));
        Verifier verifier = new Verifier(assumptions);
        do {
            Expression current = ExpressionParser.tryParseExpression(currentExpression, stringNumber);
            VerifyingResult verifyingResult = verifier.verifyExpression(current, stringNumber);
            pw.println("(" + stringNumber + ") "+ current + " (" + verifyingResult.toString() + ")");
            stringNumber++;
            currentExpression = br.readLine();
            if (currentExpression!=null)
                currentExpression = currentExpression.replaceAll("\\s+", "");
            if (!verifyingResult.isVerifyingCorrect())
                break;
        }
        while (currentExpression != null);
        pw.close();
    }
}
