package ru.ifmo.ctddev.katunina.MLogic_one_Verify_proof;

/**
 * Created by Евгения on 06.09.14.
 */
public class Implication extends BinaryOperation {
    public Implication(Expression parsedExpression, Expression expression) {
        super(parsedExpression, expression);
        sign=getSign();
    }

    @Override
    public String getSign() {
        return "->";
    }

    @Override
    protected String getStringRepresentation() {
        boolean leftBrackets = false, rightBrackets = false;
        if (left instanceof BinaryOperation && isThisPriorityHigher(sign, ((BinaryOperation) left).getSign())
                ||(left instanceof Implication))
            leftBrackets = true;
        if (right instanceof BinaryOperation && isThisPriorityHigher(sign, ((BinaryOperation) right).getSign()))
            rightBrackets = true;
        return ((leftBrackets ? "(" : "")
                + left.toString()
                + (leftBrackets ? ")" : "")
                + sign
                + (rightBrackets ? "(" : "")
                + right.toString()
                + (rightBrackets ? ")" : ""));
    }
}
