package ru.ifmo.ctddev.katunina.MLogic_one_Verify_proof;

import java.util.HashMap;

/**
 * Created by Евгения on 06.09.14.
 */
public class PropositionalVariable extends Expression {
    String name;

    public PropositionalVariable(String variable) {
        name = variable;
    }

    @Override
    boolean isTemplateFor(Expression formToCompare, HashMap<String, Expression> variable) {
        if (!variable.containsKey(name))
            variable.put(name,formToCompare);
        else
            return (formToCompare.equals(variable.get(name)));
        return true;
    }

    @Override
    protected String getStringRepresentation() {
        return name;
    }
}
